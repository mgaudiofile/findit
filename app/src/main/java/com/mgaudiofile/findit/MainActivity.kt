package com.mgaudiofile.findit

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.mgaudiofile.findit.adapters.QuestionAdapter
import com.mgaudiofile.findit.utilities.inTransaction
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val TAG: String = "MainActivity"

    private val mapFragment = MapFragment.newInstance()
    private val dashboardFragment = DashboardFragment.newInstance()
    private val notificationsFragment = NotificationsFragment.newInstance()
    private lateinit var active: Fragment

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                Toast.makeText(this, "clicked home", Toast.LENGTH_LONG).show()
                supportFragmentManager.beginTransaction().hide(active).show(mapFragment).commit()
                active = mapFragment
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                supportFragmentManager.beginTransaction().hide(active).show(dashboardFragment).commit()
                active = dashboardFragment
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                supportFragmentManager.beginTransaction().hide(active).show(notificationsFragment).commit()
                active = notificationsFragment
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.inTransaction {
                add(R.id.content_container, mapFragment)
                add(R.id.content_container, dashboardFragment).hide(dashboardFragment)
                add(R.id.content_container, notificationsFragment).hide(notificationsFragment)
            }
            active = mapFragment
        }

        val pager: ViewPager = findViewById(R.id.pager)
        pager.adapter = QuestionAdapter(supportFragmentManager)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }
}
