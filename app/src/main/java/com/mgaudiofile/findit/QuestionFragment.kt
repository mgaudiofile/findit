package com.mgaudiofile.findit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment

class QuestionFragment : Fragment() {
    private val TAG = "QuestionFragment"

    private lateinit var questionTextView: TextView

    companion object {
        private const val KEY_POSITION = "position"

        fun newInstance(position: Int): QuestionFragment {
            val questionFragment = QuestionFragment()
            val bundle = Bundle()
            bundle.putInt(KEY_POSITION, position)

            questionFragment.arguments = bundle
            return questionFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_question, container, false)
        questionTextView = fragmentLayout.findViewById(R.id.tv_question);
        val pos = arguments?.getInt(KEY_POSITION, -1)

        val res: String = "question: " + pos.toString()
        questionTextView.setText(res)

        return fragmentLayout
    }
}