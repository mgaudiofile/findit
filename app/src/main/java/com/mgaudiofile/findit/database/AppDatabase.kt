package com.mgaudiofile.findit.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(QuestionEntity::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun questionDao(): QuestionDao
}