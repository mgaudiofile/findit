package com.mgaudiofile.findit.database

import androidx.room.*

@Dao
interface QuestionDao {

    @Query("SELECT * FROM questions")
    fun getAll(): List<QuestionEntity>

    @Query("SELECT * FROM questions WHERE solved = :solved")
    fun getQuestions(solved: Boolean): List<QuestionEntity>

    @Insert
    fun insert(vararg myQuestion: QuestionEntity): LongArray

    @Update
    fun update(vararg myQuestion: QuestionEntity)

    @Delete
    fun delete(vararg myQuestion: QuestionEntity)
}