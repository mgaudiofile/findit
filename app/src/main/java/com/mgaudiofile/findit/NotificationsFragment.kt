package com.mgaudiofile.findit

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment

class NotificationsFragment : Fragment() {
    private val TAG = "NotificationsFragment"

    private lateinit var textView: TextView

    companion object {
        fun newInstance(): NotificationsFragment {
            return NotificationsFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView()")
        val fragmentLayout = inflater.inflate(R.layout.fragment_notifications, container, false)
        textView = fragmentLayout.findViewById(R.id.tv_notifications)
        textView.setText("Notifications Fragment")
        return fragmentLayout
    }
}