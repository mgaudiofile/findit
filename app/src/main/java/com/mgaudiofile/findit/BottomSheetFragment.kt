package com.mgaudiofile.findit

import android.os.Bundle
import androidx.fragment.app.Fragment


class BottomSheetFragment : Fragment() {


    /*
    The system calls this when creating the fragment.
    Within your implementation, you should initialize
    essential components of the fragment that you want
    to retain when the fragment is paused or stopped,
    then resumed.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    /*
    The system calls this when it's time for the fragment
    to draw its user interface for the first time.
    To draw a UI for your fragment, you must return a View
    from this method that is the root of your fragment's layout.
    You can return null if the fragment does not provide a UI.
     */
    //override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

    //}
}