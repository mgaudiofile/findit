package com.mgaudiofile.findit.database

class QuestionRepository private constructor(private val questionDao: QuestionDao) {

    fun getAll() = questionDao.getAll()

    fun getQuestions(solved: Boolean) = questionDao.getQuestions(solved)

    companion object {
        @Volatile private var instance: QuestionRepository? = null

        fun getInstance(questionDao: QuestionDao) =
                instance ?: synchronized(this) {
                    instance ?: QuestionRepository(questionDao).also { instance = it }
                }
    }
}