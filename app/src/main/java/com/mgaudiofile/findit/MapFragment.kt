package com.mgaudiofile.findit

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.Style

class MapFragment : Fragment() {
    private val TAG = "MapFragment"

    private var mapView: MapView? = null

    companion object {
        fun newInstance(): MapFragment {
            return MapFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView()")
        context?.let {
            Mapbox.getInstance(it, getString(R.string.mapbox_access_token))
        }

        val fragmentLayout = inflater.inflate(R.layout.fragment_map, container, false)

        mapView = fragmentLayout.findViewById(R.id.mapView)
        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync { mapboxMap -> mapboxMap.setStyle(Style.MAPBOX_STREETS) {
            mapboxMap.uiSettings.logoGravity = Gravity.TOP
            mapboxMap.uiSettings.attributionGravity = Gravity.TOP
        } }

        return fragmentLayout
    }
}