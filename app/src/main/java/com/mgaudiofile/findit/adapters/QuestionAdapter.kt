package com.mgaudiofile.findit.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.mgaudiofile.findit.QuestionFragment

class QuestionAdapter internal constructor(fm: FragmentManager): FragmentStatePagerAdapter(fm) {

    private val fragmentList = ArrayList<Fragment>()

    override fun getItem(position: Int): Fragment {
        //return fragmentList[position]
        return QuestionFragment.newInstance(position)
    }

    override fun getCount(): Int {
        //return fragmentList.size
        return 10
    }
}