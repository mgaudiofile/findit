package com.mgaudiofile.findit

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment

class DashboardFragment : Fragment() {
    private val TAG = "DashboardFragment"

    private lateinit var textMessage: TextView

    companion object {
        fun newInstance(): DashboardFragment {
            return DashboardFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView()")
        val fragmentLayout = inflater.inflate(R.layout.fragment_dashboard, container, false)
        textMessage = fragmentLayout.findViewById(R.id.tv_dashboard)
        textMessage.setText("Dashboard Fragment")
        return fragmentLayout
    }
}